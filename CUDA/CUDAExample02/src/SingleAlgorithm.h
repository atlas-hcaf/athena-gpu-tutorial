// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef CUDAEXAMPLES_SINGLEALGORITHM_H
#define CUDAEXAMPLES_SINGLEALGORITHM_H

// Gaudi include(s).
#include "GaudiKernel/ServiceHandle.h"

// Athena include(s).
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"
#include "xAODEventInfo/EventInfo.h"
#include "AthCUDAInterfaces/IStreamPoolSvc.h"

namespace AthCUDATutorial {

   /// Reentrant algorithm that performs a simple calculation on a GPU
   class SingleAlgorithm : public AthReentrantAlgorithm {

   public:
      /// Use the base class's constructor
      using AthReentrantAlgorithm::AthReentrantAlgorithm;

      /// @name Function(s) inherited from @c AthReentrantAlgorithm
      /// @{

      /// Function initialising the algorithm
      virtual StatusCode initialize() override;

      /// Function executing the algorithm for a specific event
      virtual StatusCode execute( const EventContext& ctx ) const override;

      /// @}

   private:
      /// @name Algorithm properties
      /// @{

      /// Key for reading the @c xAOD::EventInfo object of the event
      SG::ReadHandleKey< xAOD::EventInfo > m_eiKey{ this, "EventInfoKey",
         "EventInfo", "Key to read the xAOD::EventInfo object with" };

      /// Handle to the CUDA stream pool
      ServiceHandle< AthCUDA::IStreamPoolSvc > m_streamPool{ this,
         "StreamPoolSvc", "AthCUDA::StreamPoolSvc",
         "Service providing CUDA streams to the algorithm" };

      /// @}

   }; // class SingleAlgorithm

} // namespace AthCUDATutorial

#endif // CUDAEXAMPLES_SINGLEALGORITHM_H
