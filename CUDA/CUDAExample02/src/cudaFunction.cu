//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "cudaFunction.h"

// Athena include(s).
#include "AthCUDACore/StreamHolderHelpers.cuh"
#include "AthCUDACore/Macros.cuh"
#include "AthCUDACore/Memory.cuh"

// System include(s).
#include <cassert>

namespace AthCUDATutorial {

   /// Kernel function, for transforming the received arrays in some way
   __global__
   void cuArrayTransform( std::size_t arraySize, const float* a, float* b ) {

      // Make sure that the kernel is executed in 1D.
      assert( blockIdx.y == 0 ); assert( threadIdx.y == 0 );
      assert( blockIdx.z == 0 ); assert( threadIdx.z == 0 );

      // Get the array index that this thread should operate on.
      const int index = blockIdx.x * blockDim.x + threadIdx.x;
      if( index >= arraySize ) {
         return;
      }

      // Perform some transformation.
      for( int i = 0; i < 1000; ++i ) {
         b[ index ] = sin( a[ index ] * 2.0f * i ) +
                      cos( a[ index ] * 2.43f * ( i - 10 ) );
      }
   }

   void cudaFunction( const std::vector< float >& a, std::vector< float >& b,
                      AthCUDA::StreamHolder& streamHolder ) {

      // A sanity check.
      assert( a.size() == b.size() );
      const std::size_t arraySize = a.size();

      // Access the stream that we should be using.
      cudaStream_t stream = AthCUDA::getStream( streamHolder );
      assert( stream != nullptr );

      // Put the two arrays into CUDA memory.
      auto devA = AthCUDA::make_managed_array< float >( arraySize );
      auto devB = AthCUDA::make_managed_array< float >( arraySize );

      // Fill the input array.
      for( std::size_t i = 0; i < arraySize; ++i ) {
         devA.get()[ i ] = a[ i ];
      }

      // Launch the kernel.
      static const int blockSize = 256;
      const int numBlocks = ( arraySize + blockSize - 1 ) / blockSize;
      static const int sharedSize = 0;
      cuArrayTransform<<< numBlocks, blockSize, sharedSize, stream >>>(
         arraySize, devA.get(), devB.get() );

      // Check for errors, and wait for the operation to finish.
      CUDA_EXP_CHECK( cudaGetLastError() );
      CUDA_EXP_CHECK( cudaStreamSynchronize( stream ) );

      // Copy the output data into the output vector.
      for( std::size_t i = 0; i < arraySize; ++i ) {
         b[ i ] = devB.get()[ i ];
      }
   }

} // namespace AthCUDATutorial
