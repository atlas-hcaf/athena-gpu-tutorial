//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "CUDAExample03/GPUMemoryBlob.h"

// Athena include(s).
#include "AthCUDACore/TaskArena.h"

// CUDA include(s).
#include <cuda_runtime_api.h>

namespace {

   /// Functor executed by TBB to free a piece of GPU memory
   class MemoryDeleterTask {

   public:
      /// Constructor
      MemoryDeleterTask( void* ptr ) : m_ptr( ptr ) {}

      /// Operator executing the functor
      void operator()() const {
         if( cudaFree( m_ptr ) != cudaSuccess ) {
            throw std::runtime_error( "Could not free CUDA memory!" );
         }
      }

   private:
      /// Memory to be freed by the functor
      void* m_ptr;

   }; // class MemoryDeleterTask

} // private namespace

namespace AthCUDATutorial {

   GPUMemoryBlob::GPUMemoryBlob( void* ptr, std::size_t size )
   : m_ptr( ptr ), m_size( size ) {

   }

   GPUMemoryBlob::~GPUMemoryBlob() {

      AthCUDA::taskArena().enqueue( ::MemoryDeleterTask( m_ptr ) );
   }

   void* GPUMemoryBlob::ptr() {

      return m_ptr;
   }

   const void* GPUMemoryBlob::ptr() const {

      return m_ptr;
   }

   std::size_t GPUMemoryBlob::size() const {

      return m_size;
   }

} // namespace AthCUDATutorial
