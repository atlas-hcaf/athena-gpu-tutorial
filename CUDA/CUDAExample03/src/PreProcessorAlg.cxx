//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "PreProcessorAlg.h"

// Tutorial include(s).
#include "CUDAExampleCommon/cuda_sc_check.h"

// Athena include(s).
#include "StoreGate/ReadHandle.h"
#include "StoreGate/WriteHandle.h"

// CUDA include(s).
#include <cuda_runtime_api.h>

// System include(s).
#include <memory>

namespace AthCUDATutorial {

   StatusCode PreProcessorAlg::initialize() {

      // Initialise the algorithm's resources.
      ATH_CHECK( m_eiKey.initialize() );
      ATH_CHECK( m_gpuMemKey.initialize() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode PreProcessorAlg::execute( const EventContext& ctx ) const {

      // Retrieve the event info.
      SG::ReadHandle< xAOD::EventInfo > ei( m_eiKey, ctx );

      // Allocate an amount of memory based on the EventInfo properties in some
      // silly way.
      void* ptr = nullptr;
      const std::size_t size = ei->lumiBlock() * 100;
      CUDA_SC_CHECK( cudaMalloc( &ptr, size ) );

      // Create the "EDM object" that would hold on to this memory. Do this
      // already now, so that memory would not leak in case of a subsequent
      // error or early return. Since the EDM object is taking ownership of the
      // allocated memory.
      auto edmObj = std::make_unique< GPUMemoryBlob >( ptr, size );

      // Fill the memory blob in some simple way.
      CUDA_SC_CHECK( cudaMemset( edmObj->ptr(), 0, edmObj->size() ) );

      // Record the object into the event store.
      SG::WriteHandle< GPUMemoryBlob > gpuMem( m_gpuMemKey, ctx );
      ATH_CHECK( gpuMem.record( std::move( edmObj ) ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthCUDATutorial
