// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef CUDAEXAMPLES_PREPROCESSORALG_H
#define CUDAEXAMPLES_PREPROCESSORALG_H

// Local include(s).
#include "CUDAExample03/GPUMemoryBlob.h"

// Athena include(s).
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"
#include "xAODEventInfo/EventInfo.h"

namespace AthCUDATutorial {

   /// Algorithm performing a pre-processing of data for a GPU calculation
   class PreProcessorAlg : public AthReentrantAlgorithm {

   public:
      /// Use the base class's constructor
      using AthReentrantAlgorithm::AthReentrantAlgorithm;

      /// @name Function(s) inherited from @c AthReentrantAlgorithm
      /// @{

      /// Function initialising the algorithm
      virtual StatusCode initialize() override;

      /// Function executing the algorithm for a specific event
      virtual StatusCode execute( const EventContext& ctx ) const override;

      /// @}

   private:
      /// @name Algorithm properties
      /// @{

      /// Key for reading the @c xAOD::EventInfo object of the event
      SG::ReadHandleKey< xAOD::EventInfo > m_eiKey{ this, "EventInfoKey",
         "EventInfo", "Key to read the xAOD::EventInfo object with" };

      /// Key for writing the @c AthCUDATutorial::GPUMemoryBlob object with
      SG::WriteHandleKey< GPUMemoryBlob > m_gpuMemKey{ this, "GPUMemoryKey",
         "GPUMemory",
         "Key to write the AthCUDATutorial::GPUMemoryBlob object with" };

      /// @}

   }; // class PreProcessorAlg

} // namespace AthCUDATutorial

#endif // CUDAEXAMPLES_PREPROCESSORALG_H
