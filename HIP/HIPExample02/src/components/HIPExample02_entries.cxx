//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "../SingleAlgorithm.h"

// Declare the components to Gaudi.
DECLARE_COMPONENT( AthHIPTutorial::SingleAlgorithm )
