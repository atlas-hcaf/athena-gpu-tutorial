// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef HIPEXAMPLES02_HIPFUNCTION_H
#define HIPEXAMPLES02_HIPFUNCTION_H

// System include(s).
#include <vector>

namespace AthHIPTutorial {

   /// Simple function doing "something" using CUDA
   void hipFunction( const std::vector< float >& a, std::vector< float >& b );

} // namespace AthHIPTutorial

#endif // HIPEXAMPLES02_HIPFUNCTION_H
